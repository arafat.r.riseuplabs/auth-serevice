<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rules\Password;

class UserLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|string|min:10|max:255',
            'password' => [
                'required', 'string', 'max:255',
                Password::min(8)->mixedCase()->symbols()->numbers()->uncompromised(),
            ],
        ];
    }

    public function messages()
    {
        return [
            'password.regex' => 'The password must contain at least one lowercase letter, one uppercase letter, one digit, one special character',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @throws \Illuminate\Validation\ValidationException|Illuminate\Http\Exceptions\HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        if ($this->expectsJson()  || $this->is('api/*')) {
            $errors = $validator->errors()->toArray();
            $response = response()->json(['status' => false, 'message' => 'Invalid credentials.', 'errors' => $errors], 422);
            throw new HttpResponseException($response);
        }
        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
