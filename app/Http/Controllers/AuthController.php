<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegistrationRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(UserLoginRequest $request)
    {
        try {
            $inputs = $request->only('email', 'password');
            if (Auth::attempt($inputs)) {
                $authUser = Auth::user();

                $api_token = $authUser->createToken('authToken')->plainTextToken;
                $user = new UserResource(Auth::user());

                return successResponse('Login Successful.', $user, 200, $api_token);
            } else {
                return errorResponse('Your email or password is incorrect.');
            }
        } catch (Exception $ex) {
            return errorResponse(getErrorMessage($ex), null, $ex->getCode());
        }
    }

    public function register(UserRegistrationRequest $request)
    {
        try {

            $inputs = $request->except('password');
            $inputs['password'] = Hash::make($request->password);
            $user = User::create($inputs);
            $user = new UserResource($user);

            return successResponse('User Registration Successful.', $user, 201);
        } catch (Exception $ex) {
            return errorResponse(getErrorMessage($ex), null, $ex->getCode());
        }
    }

    public function logout()
    {
        try {
            $user = Auth::user();

            $user->currentAccessToken()->delete();
            return successResponse('Logout Successful.');
        } catch (Exception $ex) {
            return errorResponse(getErrorMessage($ex), null, $ex->getCode());
        }
    }

    public function isValidate()
    {
        try {
            $authUser = Auth::user();
            if ($authUser) {
                $user = new UserResource(Auth::user());
                return successResponse('User is valid.', $user, 200);
            } else {
                return errorResponse('Unauthorize User.', null, 401);
            }
        } catch (Exception $ex) {
            return errorResponse(getErrorMessage($ex), null, $ex->getCode());
        }
    }
}
