<?php

use App\Enums\BlogEnum;
use App\Http\Resources\UserResource;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;



/**
 * @param $message
 * @param $data
 * @param int $code
 * 
 * 
 */
function successResponse($message = null, $data = null, int $code = 200, $token = null)
{
    $data =
        [
            'status' => 'success',
            'message' => $message,
            'token' => $token,
            'data' => $data,
        ];
    if (!$token) {
        unset($data['token']);
    };

    return response()->json($data, $code);
}

/**
 * @param $message
 * @param $data
 * @param int $code
 *
 * 
 */
function errorResponse($message = null, $data = null, int $code = 400)
{
    return response()->json([
        'status' => 'error',
        'message' => $message,
        'data' => $data
    ], $code);
}

/**
 * @param Exception $exception
 * @return string
 * 
 */
function getErrorMessage(Exception $exception)
{
    if (config('app.debug')) {
        return $exception->getMessage() . ' on line ' . $exception->getLine() . ' in file ' . $exception->getFile();
    }
    return 'Something went wrong. Please try again later.';
}

/**
 * @param $date
 * @param string $format
 * @return string|null
 * 
 */
function dateFormat($date, string $format = 'd-m-Y')
{
    try {
        return $date ? date($format, strtotime($date)) : null;
    } catch (Exception $e) {
        return null;
    }
}
