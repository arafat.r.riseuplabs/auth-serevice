<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()->create([
            'name' => 'Mr. User',
            'email' => 'user@gmail.com',
            'password' => bcrypt('/user1&@*iL'),
        ]);

        User::factory()->count(20)->create();
    }
}
